var enemy = require('./visualize');
var debug = require('debug')('Ditto:Hitmonchan:index');

module.exports = {
	visualize: function(client, config_module, req, res, next) {
		debug("Enemy");
		enemy.visualize(client, config_module, req, res, next);
	},
	prepare_punch: function(api, uri, method, info){
		debug("prepare_punch");
		return require('./prepare_punch')(api, uri, method, info);
	}
}