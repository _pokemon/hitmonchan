var exports = module.exports = {};

exports.error = function(res, type, prefix, message, code, error) {
  if (prefix == '') {
    prefix = '[Hitmonchan][error][connect] ->';
  }

  res.status(code);
  res.render(type, {
    message: prefix + message,
    error: error
  });
}