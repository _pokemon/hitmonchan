var request = require('request');
var redis = require('redis');
var moment = require('moment');
var debug = require('debug')('Ditto:Hitmonchan:Visualize');
var config_app = null;
var config_api = null;
var config_redis = null;
var error_app = require('./error.js');
var client = null;
var exports = module.exports = {};


exports.visualize = function(redis, config, req, res, next) {
    client = redis;
    config_app = config.app;
    config_api = config.api;
    config_redis = config.redis;
    var host = config_api.host;
    var protocol = config_api.protocol.https ? 'https://' : 'http://';
    var port = config_api.protocol.port;
    var version = config_api.connect.version;
    var resource = config_api.connect.resource;
    var endpoint = config_api.connect.endpoint;
    var app_id = config_app.id;
    var token = config_app.token;

    var uri = protocol + host + ':' + port + '/' + version + '/' + resource + '/' + endpoint + '?app_id=' + app_id;

    var options = {
        method: config_api.connect.method,
        url: uri,
        headers: {
            'Authorization': 'Token token=' + token
        }
    };

    client.get(config.redis.session.keys.app, function(err, reply) {

        debug('Read session from redis');
        if (reply == null) {
            debug('No session in redis, now go to hitmonchan.');
            connexion_with_hitmonchan(options, req, res, next);
        } else {
            try {
                app = JSON.parse(reply);

                var expired = moment(app.data.expiration_date, 'YYYY-MM-DD');
                var now = moment();

                debug('Validate expired of session.');

                if (expired.diff(now, 'hours') > 0) {
                    debug('Session valid');
                    req.app.set('app', app);
                    next();
                } else {
                    debug('Session expired, now go to hitmonchan.');
                    connexion_with_hitmonchan(options, req, res, next);
                }
            } catch (e) {
                debug('Exception: ' + e.message);
                error_app.error(res, 'error', '', e, 500, {});
            }
        }
    });
}

function connexion_with_hitmonchan(options, req, res, next) {
    debug('Connexion with hitmonchan.');
    debug('Options of connexion.');
    debug(options);

    request(options, function(error, response, body) {
        if (!error) {
            try {
                app = JSON.parse(body);
                if (response.statusCode == 200 || response.statusCode == 201) {
                    if (app.status) {
                        client.set(config_redis.session.keys.app, body, redis.print);
                        req.app.set('app', app);
                        debug('Connexion Success.');
                        next();
                    } else {
                        debug('Error on status: ' + app.status);
                        debug(app);
                        error_app.error(res, 'error', '', app.status, 500, {});
                    }
                } else {
                    debug('Error StatusCode: ' + response.statusCode);
                    error_app.error(res, 'error', '', app.message, app.status, {});
                }
            } catch (e) {
                debug('Exception: ' + e.message);
                error_app.error(res, 'error', '', body, 500, {});
            }
        } else {
            debug('Error on request: ' + error);
            error_app.error(res, 'error', '', error, 500, {});
        }

    });
}
