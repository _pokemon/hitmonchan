var config_app = null;
var debug = require('debug')('Ditto:Hitmonchan:Prepare_punch');
var isObject = require('isobject');
var fs = require('fs');

module.exports = function(api, uri, method, info) {
	var options = {};
	if (typeof uri == 'string') {
		var protocol = api.protocol.https ? 'https://' : 'http://';
		var host = api.host;
		var port = api.protocol.port;
		var uri = protocol + host + ':' + port + '/' + uri;
		
		if (info.file instanceof Array && Object.keys(info.file).length > 0 && isObject(info.data) && isObject(info.query)) {
			info.data['file'] = fs.createReadStream(info.file[0].path);
			options = {
				method: method,
				url: uri,
				formData: info.data,
				qs: info.query
			};
		} else if (info.file instanceof Array && Object.keys(info.file).length > 0 && isObject(info.data)) {
			info.data['file'] = fs.createReadStream(info.file[0].path);
			options = {
				method: method,
				url: uri,
				formData: info.data
			};
		} else if (info.file instanceof Array && Object.keys(info.file).length > 0 && isObject(info.query)) {
			info.data['file'] = fs.createReadStream(info.file[0].path);
			options = {
				method: method,
				url: uri,
				formData: info.data,
				qs: info.query
			};
		} else if (info.file instanceof Array && Object.keys(info.file).length > 0) {
			info.data['file'] = fs.createReadStream(info.file[0].path);
			options = {
				method: method,
				url: uri,
				formData: info.data
			};
		} else if (isObject(info.data) && isObject(info.query)) {
			options = {
				method: method,
				url: uri,
				formData: info.data,
				qs: info.query
			};
		} else if (isObject(info.query)) {
			options = {
				method: method,
				url: uri,
				qs: info.query
			};
		} else {
			debug('Problem with the arguments');
			debug(info.file);
			debug(info.data);
			debug(info.query);
		}
	} else {
		error = ' resouce: ' + (typeof resource) + ' | endpoint: ' + (typeof endpoint);
		console.log(error);
	}

	return options;
}